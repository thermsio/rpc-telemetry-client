## [3.0.1](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v3.0.0...v3.0.1) (2024-03-18)


### Bug Fixes

* remove server deployed rpc status ([48b1618](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/48b16181da3242ba678417859a8f2adda006f0eb))

# [3.0.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v2.4.2...v3.0.0) (2024-03-17)


### Features

* add window.TELEMETRY_SERVER_URL for api data fetching and removed hard coded therms andbox url ([e677102](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/e6771025c5b6a7cac087435b46a5edb9bf7fee6c))


### BREAKING CHANGES

* The window.TELEMETRY_SERVER_URL needs to be set when the index.html page is served.

## [2.4.2](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v2.4.1...v2.4.2) (2023-12-18)


### Bug Fixes

* add better logging to useDownloadTSConvertedSchema() when it errors ([a1b1e28](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/a1b1e28dce8bf89c9db5a6e8279630506c472f70))

## [2.4.1](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v2.4.0...v2.4.1) (2023-11-08)


### Bug Fixes

* quicktype/docs hack to make all JSON-schema objects additionalProperties: [secure] ([7f1a033](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/7f1a033b16cf7f80dd2d0589a238e501718b4b4c))

# [2.4.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v2.3.0...v2.4.0) (2023-11-08)


### Bug Fixes

* update deps ([d7fc210](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/d7fc210887ff5d16c460d7815012c98a432a08f2))


### Features

* add opts to rpcService calls ts schema file download ([ab226f0](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/ab226f0fb8ec4a98de07790b0f784ff71882b528))

# [2.3.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v2.2.1...v2.3.0) (2023-08-31)


### Features

* shorten call to use type in TS doc download file ([13399e4](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/13399e4b7a7602c059d64b00746303182baeec72))

## [2.2.1](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v2.2.0...v2.2.1) (2022-12-15)


### Bug Fixes

* add noinex to html ([dd28cbd](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/dd28cbdd26e32f092623e919a8519ff219995ec6))

# [2.2.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v2.1.0...v2.2.0) (2022-11-19)


### Features

* update deps ([0fc514c](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/0fc514c92088b80287dfbf62a4c9894a1b173f7d))

# [2.1.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v2.0.2...v2.1.0) (2022-09-21)


### Features

* update deps ([e6245f0](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/e6245f0ec79a12316e461920372ed2017ba7a08b))

## [2.0.2](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v2.0.1...v2.0.2) (2022-05-25)


### Bug Fixes

* update sandbox domain ([b2f0d8f](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/b2f0d8f3774ce63800d3130550e4f90b8c789893))

## [2.0.1](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v2.0.0...v2.0.1) (2022-05-21)


### Bug Fixes

* update deps ([a52abca](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/a52abca4ce850debb1fc23657e4e65339df3bcff))

# [2.0.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.17.1...v2.0.0) (2022-04-14)


### Bug Fixes

* update deps ([f3f4448](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/f3f444833b99d39a5c21a8b8ed823c2706a3a758))


### BREAKING CHANGES

* next branch merged all changed

## [1.17.1](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.17.0...v1.17.1) (2022-02-08)


### Bug Fixes

* RPCService export ([339191c](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/339191cda95a18904f2ba8dca58aa7da61e89c6e))

# [1.17.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.16.2...v1.17.0) (2022-02-08)


### Features

* RPCService export replaces rpc export in downloaded TS ([5df71a6](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/5df71a60b3202ec73c535d019d318c0f5432b6af))

## [1.16.2](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.16.1...v1.16.2) (2022-01-24)


### Bug Fixes

* menu procedure versions duplicates check ([b79e877](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/b79e877ecb2f7eef4ee530a5d9cbefb28d634fea))

## [1.16.1](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.16.0...v1.16.1) (2022-01-15)


### Bug Fixes

* rpc caller builder window.rpcClient func ([f1afc7e](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/f1afc7e1f650b267feb4387ac5648c3dfff5629c))

# [1.16.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.15.0...v1.16.0) (2022-01-15)


### Features

* add rpc caller codegenerated for rpcClient ([da5043d](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/da5043d5353b32b2d6f41d5406e8dde1b30d9531))

# [1.15.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.14.0...v1.15.0) (2022-01-01)


### Features

* improve regex search menu ([c320186](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/c320186af4e408fc3d3ea6ad5c27cd81ac4aa300))

# [1.14.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.13.0...v1.14.0) (2021-12-29)


### Features

* rename RPC constants TS types export name to RPCRequests ([f0ee69c](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/f0ee69c69f2590853763606e2e3b048ebb4ef9ed))

# [1.13.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.12.1...v1.13.0) (2021-12-29)


### Features

* add RPC constants export for TS download ([20f69e0](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/20f69e03b1f8f3d9bb992d3b58eeacc76e100ad0))

## [1.12.1](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.12.0...v1.12.1) (2021-12-27)


### Bug Fixes

* typescript schema file export to use startcase ([c3783d6](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/c3783d62c25c2c63455b7bf066399f61da9cbfea))

# [1.12.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.11.2...v1.12.0) (2021-12-27)


### Features

* add download all typescript schema into a single file ([963c406](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/963c4066d1ab6b0ddec6142e16b39e4c7b6a6f77))

## [1.11.2](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.11.1...v1.11.2) (2021-12-27)


### Bug Fixes

* copy text ([280f892](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/280f89283fc60be6d4377e4b15b44be6587e4c5a))

## [1.11.1](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.11.0...v1.11.1) (2021-12-27)


### Bug Fixes

* Swift quicktype conversion acronym-style for ID->Id ([980dbf5](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/980dbf5c886d825d32d6ac1e4d10336bda01c70c))

# [1.11.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.10.1...v1.11.0) (2021-12-25)


### Features

* add ErrorBoundary wrappers and reformat jsonSchemaType name in monaco editor ([16aff04](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/16aff0423dc5e221041d255bfba5fc035e049081))

## [1.10.1](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.10.0...v1.10.1) (2021-12-25)


### Bug Fixes

* move all deps to devdeps ([5fa32ea](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/5fa32eac2904b68b0375b3236e8ed70c1ff19556))
* move all deps to devdeps ([414bf82](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/414bf82d90850f652f0415063cea3037a9127595))
* move all deps to devdeps ([285a216](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/285a216f186800ebcd677698ad7b75ea4e73b75f))

# [1.10.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.9.1...v1.10.0) (2021-12-25)


### Bug Fixes

* bitbucket-pipelines ([b1d4726](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/b1d4726a38863bcddd4d5eb3e3a6ba90a381504c))
* dangle comma dangler ([5263c07](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/5263c07038dd2f3100f01a4eef22dcd64af82f1b))
* import ([3dfb483](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/3dfb483c63cd27dec2d0594784886e9266130aa0))
* import capitalized filename ([4272127](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/4272127a4d6354c0b33a3b9ee5f616de6f398877))
* imports ([5932c80](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/5932c800879cd66d8926b96c3a98a0020c9de18f))
* package-lock ([a2af7d8](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/a2af7d82ac20703c3fecc191f16f99c2ca398a31))
* package.json test command ([911ec09](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/911ec093d70b7e0a22bc934f7f76bceac690e1eb))


### Features

* **CORE:** Refactored variable names ([afc585c](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/afc585ca51e5b635f0b51f7155c4654e36ea811d))
* **CORE-1723:** Installed axios ([11c88a1](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/11c88a18659ec77a54cc07932f8b7faf2f80a774))
* **CORE-1723:** WIP Retrieving and storing rpc docs data ([0cba8b2](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/0cba8b217dca9c3acafc98c374bc05276bded1ad))
* **CORE-1725:** WIP Displaying Doc scopes/procedures ([bd94d3f](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/bd94d3ffbe1bbabde7bae43df6b4620450d1697c))
* **CORE-1725:** WIP DocsMenu layout stubbed ([23e3f56](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/23e3f5697e7d9134affc50e12fae7e3346791b40))
* **CORE-1726:** Docs details container ([0cbbac5](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/0cbbac5f11a4d2d0c4d8250cda8f8f42e0830f2e))
* **CORE-1726:** Fixed typo and fixed width issue ([5a8500e](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/5a8500ee464a1e05e0acf8bcebbc137ec6e6f5a5))
* **CORE-1726:** Removed commented out code and console logs ([4a13e4d](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/4a13e4dd42c23fe50a16a0ae31123bd61574ab3f))
* **CORE-1726:** Removed console logs ([a50db92](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/a50db925311edca13800e23e0b331a34a6a16910))
* **CORE-1726:** WIP Displaying RPC info ([611135d](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/611135d03fe5230c91bd1a6870698c477c1711f9))
* **CORE-1726:** WIP RPCDetail component ([fa3ed20](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/fa3ed206c45db9eeed036b21184120c678cd74d4))
* **CORE-1743:** Copy JSON schema to clipboard ([68eb0fb](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/68eb0fb976a52b9b215b8de72a94394c9d7abbb8))
* **CORE-1743:** Created CopyJSONSchema component ([0417c5b](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/0417c5b51bdd936b6ea35748c264d2d35ba6a59d))
* **CORE-1745:** Refactored searchbar ([393befa](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/393befaf603d97e3674b1027acecf71e7fb49513))
* **CORE-1745:** Refactored styling ([6f7f7b1](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/6f7f7b1439fb6f012717672d6b50e1a8632b39c2))
* **CORE-1745:** Searchbar and Server screen ([91935cb](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/91935cbc65a044e9a59f96fa104eb742120a235c))
* **CORE-1745:** WIP ([9eee9ec](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/9eee9ecb49612e49c0992bb70470a9349fcd5b0f))
* **CORE-1748:** Added Args & Data Tabs ([84c0c4b](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/84c0c4b4c2a7c97072c81e6b4cb12e1cc5544fde))
* **CORE-1749:** WIP Adding Monaco ([3179302](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/31793022b4e61e32a8ed408b8e793e4f61ff8044))

## [1.9.1](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.9.0...v1.9.1) (2021-05-20)


### Bug Fixes

* sort sidebar docs by scope and procedure ([98ff1c2](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/98ff1c2f90ed4b1d225ac5c973435a2ea926604d))

# [1.9.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.8.0...v1.9.0) (2021-05-15)


### Features

* json formatter ([1f3050a](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/1f3050a15927f79db8e9402748f44504a58c4c41))

# [1.8.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.7.6...v1.8.0) (2021-05-06)


### Features

* server internal icons and improve procedure doc pages ([c7ee834](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/c7ee834e9fc0a07d19db390750aae3cfa0940053))

## [1.7.6](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.7.5...v1.7.6) (2021-05-05)


### Bug Fixes

* package.json main path ([3b0c124](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/3b0c124636d8bb259759364c9156040d9f7a9f5d))

## [1.7.5](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.7.4...v1.7.5) (2021-05-05)


### Bug Fixes

* move all deps to devdeps ([e19d613](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/e19d613ccf3d1d7716fc2689b57bf1e9315f8726))

## [1.7.4](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.7.3...v1.7.4) (2021-05-05)


### Bug Fixes

* getHostUrlPath ([412a554](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/412a5540522f9c336d1ed6c37423807f2f30814c))

## [1.7.3](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.7.2...v1.7.3) (2021-05-05)


### Bug Fixes

* update base url logic ([988c429](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/988c42902a2cb3dd2504fe611f26b45611733b03))

## [1.7.2](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.7.1...v1.7.2) (2021-05-05)


### Bug Fixes

* git ignore ([3f871c6](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/3f871c64f1a32ba904331e0c7a1785e11d754b15))
* use location url for rpc telemetry server address if no env var provided ([eb9f4d9](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/eb9f4d9b9df3049546d5ce40d53414a6700ae096))

## [1.7.1](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.7.0...v1.7.1) (2021-05-05)


### Bug Fixes

* publish dirs ([c44c4d5](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/c44c4d565f250d71d18e936b4507ad7b07bb516b))

# [1.7.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.6.0...v1.7.0) (2021-05-05)


### Features

* add mobile navigation and readme ([d3a3621](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/d3a36211d58e773cb3a416807177e5d9da62856b))

# [1.6.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.5.0...v1.6.0) (2021-05-05)


### Features

* docs server online status ([e4240ee](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/e4240eece9bbf4f0bcad972f84dbe83b43b30302))

# [1.5.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.4.0...v1.5.0) (2021-05-05)


### Features

* docs displaying and navigation working ([9a664ac](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/9a664acc7e3b659feebda79cc0ec32caa49807b8))

# [1.4.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.3.0...v1.4.0) (2021-05-04)


### Features

* data fetching for docs on rpc.sandbox.therms ([8b5b4ec](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/8b5b4ec802b0aa3ef97713b90d6c83e3b416ae07))

# [1.3.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.2.0...v1.3.0) (2021-05-04)


### Features

* wip ([0d88ff1](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/0d88ff1eebbf0bec50571dc0350faa8ca5ca8f79))

# [1.2.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.1.0...v1.2.0) (2021-03-28)


### Features

* wip ([fd337ed](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/fd337edbb6cd70beb03680db1eac39b6b2ba4e4e))

# [1.1.0](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.0.2...v1.1.0) (2021-03-28)


### Features

* react app wip ([b8808bf](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/b8808bf59e4658a34388979469a699d550bdf593))

## [1.0.2](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.0.1...v1.0.2) (2021-03-24)


### Bug Fixes

* npmignore ([9a15c32](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/9a15c328fbf125f9820a11569af8ce3edae7e40c))

## [1.0.1](http://bitbucket.org/thermsio/rpc-telemetry-client/compare/v1.0.0...v1.0.1) (2021-03-24)


### Bug Fixes

* build public path ([5f47b5c](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/5f47b5c3ecc7758513f9cfc8cad8d2240e5f8b5c))

# 1.0.0 (2021-03-24)


### Bug Fixes

* build script ([eeab0ee](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/eeab0eed7e31bf7e0807d8bb83946a07f95a8820))


### Features

* initial commit and skeleton ([7458b38](http://bitbucket.org/thermsio/rpc-telemetry-client/commits/7458b38209e4e8dda554411cc5a90067b38f5a84))
