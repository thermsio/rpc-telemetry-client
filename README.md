# @therms/rpc-telemetry-client

![rpc_logo.png](https://bitbucket.org/thermsio/rpc-server-ts/raw/a9f154178cbc69e9b821ed22fbe7bfec125cdf86/rpc_logo.png)

A _Remote Procedure Call_ framework for Javascript Node.js written in TS.

- JS/TS client (https://bitbucket.org/thermsio/rpc-client-ts)
- Android/Kotlin client (https://bitbucket.org/thermsio/rpc-client-kotlin)
- iOS/Swift client (https://bitbucket.org/thermsio/rpc-client-swift)

 ----------

### https://www.rpc-docs.dev

This is the web UI for viewing RPC servers and RPC documentation that is served by a Telemetry Server 
from the `@therms/rpc-server` package.

### RPC Docs Telemetry Server

This is the web-app for viewing RPC docs from a rpc-server. It loads the docs from the rpc-server and 
allows the user to view and search the RPC docs.
