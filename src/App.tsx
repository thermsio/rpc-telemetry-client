import { RootRoute } from './routes/RootRoute.tsx';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import './App.css';
import { RPCDocsRoute } from '@/routes/RPCDocsRoute.tsx';
import { Toaster } from '@/components/ui/sonner';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootRoute />,
  },
  {
    path: '/rpc/:scope?/:procedure?/:version?',
    element: <RPCDocsRoute />,
  },
]);

function App() {
  return (
    <>
      <RouterProvider router={router} />
      <Toaster />
    </>
  );
}

export default App;
