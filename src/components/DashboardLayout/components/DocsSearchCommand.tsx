import {
  Command,
  CommandEmpty,
  CommandGroup,
  CommandInput,
  CommandItem,
  CommandList,
  CommandDialog,
} from '@/components/ui/command.tsx';
import { useDocsStore } from '@/state/docs.ts';
import { Badge } from '@/components/ui/badge.tsx';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';

function DocsSearchCommand() {
  const navigate = useNavigate();

  const {
    docsSearchResults,
    docsSearchShowDialog,
    docsSearchText,
    setDocsSearchShowDialog,
    setDocsSearchText,
  } = useDocsStore();

  useEffect(() => {
    const down = (e: KeyboardEvent) => {
      if (e.key === 'k' && (e.metaKey || e.ctrlKey)) {
        e.preventDefault();
        setDocsSearchShowDialog(!docsSearchShowDialog);
      }
    };
    document.addEventListener('keydown', down);
    return () => document.removeEventListener('keydown', down);
  }, []);

  return (
    <CommandDialog
      open={docsSearchShowDialog}
      onOpenChange={setDocsSearchShowDialog}
    >
      <Command className="rounded-lg border shadow-md" shouldFilter={false}>
        <CommandInput
          onValueChange={setDocsSearchText}
          placeholder="Search all docs..."
          value={docsSearchText}
        />
        <CommandList>
          <CommandEmpty>No results found.</CommandEmpty>
          {docsSearchResults.map((result, i) => {
            // when the result.scope is the same as the previous don't show the heading
            const showScopeHeading =
              i === 0 || docsSearchResults[i - 1].scope !== result.scope;

            return (
              <CommandGroup
                className="p-0"
                key={result.id}
                heading={
                  showScopeHeading && (
                    <div className="text-lg font-semibold">{result.scope}</div>
                  )
                }
              >
                <CommandItem
                  onSelect={() => {
                    navigate(
                      `/rpc/${result.scope}/${result.procedure}/${result.version}`,
                    );
                    setDocsSearchShowDialog(false);
                  }}
                >
                  <div className="flex flex-col w-full ml-4">
                    <div className="flex justify-between">
                      <span>{result.procedure}</span>
                      <Badge>{result.version}</Badge>
                    </div>

                    <p className="text-gray-600 font-light text-xs italic">
                      {result.description}
                    </p>
                  </div>
                </CommandItem>
              </CommandGroup>
            );
          })}
        </CommandList>
      </Command>
    </CommandDialog>
  );
}

DocsSearchCommand.defaultProps = {};

export { DocsSearchCommand };
