import { Button } from '@/components/ui/button';
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuGroup,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from '@/components/ui/dropdown-menu';
import { IoMenu } from 'react-icons/io5';
import { downloadTypescriptSchema } from '@/util/download-typescript-schema.ts';
import { useDocsStore } from '@/state/docs.ts';
import { downloadJson } from '@/util/download-json.ts';

interface RPCContextMenuProps {}

function RPCContextMenu({}: RPCContextMenuProps) {
  const docsStore = useDocsStore();
  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <Button className="px-1" size="sm">
          <IoMenu size={32} />
        </Button>
      </DropdownMenuTrigger>

      <DropdownMenuContent className="w-56">
        <DropdownMenuLabel>Download</DropdownMenuLabel>
        <DropdownMenuSeparator />
        <DropdownMenuGroup>
          <DropdownMenuItem
            onClick={() => downloadTypescriptSchema(docsStore.rpcsByScope)}
          >
            <span>Typescript Schema Types</span>
          </DropdownMenuItem>
          <DropdownMenuItem
            onClick={() => downloadJson(docsStore.rpcDocs)}
          >
            <span>JSON RPC Definitions</span>
          </DropdownMenuItem>
        </DropdownMenuGroup>
      </DropdownMenuContent>
    </DropdownMenu>
  );
}

RPCContextMenu.defaultProps = {};

export { RPCContextMenu };
