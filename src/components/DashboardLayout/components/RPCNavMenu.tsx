import { ImSpinner10 } from 'react-icons/im';
import { Input } from '@/components/ui/input.tsx';
import { NavLink } from 'react-router-dom';
import { TRPCsByScope, useDocsStore } from '@/state/docs.ts';
import { useEffect, useMemo, useRef, useState } from 'react';
import { useParams } from 'react-router';
import { usePrevious } from 'ahooks';

interface RPCNavMenuProps {}

function RPCNavMenu({}: RPCNavMenuProps) {
  const { isLoadingDocs, loadDocsError, rpcsByScope } = useDocsStore();
  const { scope, procedure } = useParams<{
    scope?: string;
    procedure?: string;
    version?: string;
  }>();
  const [filter, setFilter] = useState('');
  const previousFilter = usePrevious(filter);

  const rpcsByScopeList = useMemo(() => {
    if (filter) {
      const rgxs = filter
        .replace(/[^a-zA-Z0-9]/gi, ' ')
        .split(/ /gi)
        .map((term) => {
          return new RegExp(term, 'gi');
        });

      const result = Object.entries(rpcsByScope).reduce((acc, [_, rpcs]) => {
        rpcs.forEach((rpc) => {
          let matchedTermCount = 0;

          for (const rgx of rgxs) {
            // console.log('`${rpc.scope} ${rpc.procedure}`', `${rpc.scope} ${rpc.procedure}`)
            if (rgx.test(rpc.scope) || rgx.test(rpc.procedure)) {
              matchedTermCount++;
            }
          }

          if (matchedTermCount === rgxs.length) {
            acc[rpc.scope] ??= [];
            acc[rpc.scope].push(rpc);
          }
        });

        return acc;
      }, {} as TRPCsByScope);

      return Object.entries(result);
    }

    return Object.entries(rpcsByScope).sort(([a], [b]) => a.localeCompare(b));
  }, [rpcsByScope, filter]);

  const navRef = useRef<HTMLDivElement>(null);
  const navHeight =
    window.innerHeight - (navRef.current?.getBoundingClientRect().top ?? 0);

  const local = useRef<{
    scopeRefs: Record<string, HTMLDivElement | null>;
    proceduresByScopeRefs: Record<
      string,
      Record<string, HTMLAnchorElement | null>
    >;
  }>({ scopeRefs: {}, proceduresByScopeRefs: {} });

  const scrollToScope = (scope: string) => {
    local.current.scopeRefs[scope]?.scrollIntoView({
      behavior: 'smooth',
      block: 'nearest',
    });
  };

  const scrollToProcedure = (scope: string, procedure: string) => {
    local.current.proceduresByScopeRefs?.[scope]?.[procedure]?.scrollIntoView({
      behavior: 'smooth',
      block: 'nearest',
    });
  };

  useEffect(() => {
    if (isLoadingDocs) return;

    if (previousFilter && !filter) {
      // scroll to rpc when the filter is cleared
      if (scope && procedure) scrollToProcedure(scope, procedure);
      else if (scope) scrollToScope(scope);
    }
  }, [filter, isLoadingDocs]);

  useEffect(() => {
    if (scope && procedure && !isLoadingDocs)
      scrollToProcedure(scope, procedure);
    else if (scope && !isLoadingDocs) scrollToScope(scope);
  }, [isLoadingDocs, navRef.current, scope, procedure]);

  return (
    <div className="flex flex-col overflow-hidden min-h-0 max-w-[300px] ">
      {isLoadingDocs && (
        <div className="flex flex-col py-2 items-center space-y-4">
          <ImSpinner10 className="animate-spin" size={24} />
          <div className="text-gray-500 text-sm">Loading Docs...</div>
        </div>
      )}

      {loadDocsError && (
        <div className="flex flex-col py-2 items-center space-y-4">
          <div className="text-red-500 text-sm">
            {loadDocsError.message
              ? loadDocsError.message
              : 'Failed to load docs'}
          </div>
        </div>
      )}

      {!loadDocsError && !isLoadingDocs && (
        <>
          <div className="p-2 pb-2">
            <Input
              className="w-full bg-white shadow-none appearance-none"
              onChange={(e) => setFilter(e.target.value)}
              placeholder="Filter RPC's..."
              type="search"
              value={filter}
            />
          </div>

          <nav
            className="px-4 text-sm overflow-scroll mt-2"
            ref={navRef}
            style={{ height: `${navHeight}px` }}
          >
            {rpcsByScopeList.map(([scope, rpcs]) => (
              <div
                key={scope}
                className="mb-2"
                ref={(ref) => (local.current.scopeRefs[scope] = ref)}
              >
                <NavLink
                  className={({ isActive }) => {
                    let classes = `p-2 rounded flex`;
                    if (isActive) classes += ' bg-gray-200';
                    return classes;
                  }}
                  to={`/rpc/${scope}`}
                >
                  <div className="font-bold">{scope}</div>
                </NavLink>

                <ul className="pl-2">
                  {rpcs.map((rpc) => (
                    <NavLink
                      className={({ isActive }) => {
                        let classes = `p-2 rounded flex items-center`;
                        if (isActive) classes += 'font-medium bg-gray-200/60';
                        return classes;
                      }}
                      key={rpc.procedure}
                      ref={(ref) => {
                        local.current.proceduresByScopeRefs[scope] ??= {};
                        local.current.proceduresByScopeRefs[scope][
                          rpc.procedure
                        ] = ref;
                      }}
                      to={`/rpc/${scope}/${rpc.procedure}`}
                    >
                      <span>{rpc.procedure}</span>
                    </NavLink>
                  ))}
                </ul>
              </div>
            ))}
          </nav>
        </>
      )}
    </div>
  );
}

RPCNavMenu.defaultProps = {};

export { RPCNavMenu };
