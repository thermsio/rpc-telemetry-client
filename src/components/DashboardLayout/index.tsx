import { PropsWithChildren, useRef } from 'react';
import { FaServer } from 'react-icons/fa6';
import { FaSearch } from 'react-icons/fa';
import { Input } from '@/components/ui/input.tsx';
import { ServerField } from '@/components/ServerField.tsx';
import { RPCNavMenu } from '@/components/DashboardLayout/components/RPCNavMenu.tsx';
import { useDocsStore } from '@/state/docs.ts';
import { ImSpinner10 } from 'react-icons/im';
import { DocsSearchCommand } from '@/components/DashboardLayout/components/DocsSearchCommand.tsx';
import { CommandShortcut } from '@/components/ui/command.tsx';
import { RPCContextMenu } from '@/components/DashboardLayout/components/RPCContextMenu.tsx';

interface DashboardLayoutProps extends PropsWithChildren {}

function DashboardLayout({ children }: DashboardLayoutProps) {
  const { rpcDocs, isLoadingDocs, setDocsSearchShowDialog, docsSearchText } =
    useDocsStore();

  const mainRef = useRef<HTMLDivElement>(null);

  const mainHeightToBottomOfScreen =
    window.innerHeight - (mainRef.current?.getBoundingClientRect().top ?? 0);

  return (
    <>
      <DocsSearchCommand />

      <div className="w-screen h-screen overflow-hidden flex flex-col">
        <div className="border-b-2 border-gray-100/50 flex w-full py-2">
          <div className="w-[300px] flex justify-between px-2">
            <div className="flex items-center space-x-2">
              <FaServer className="h-6 w-6" />
              <span className="text-lg font-semibold">RPC Docs</span>
            </div>
            {!!rpcDocs.length && <RPCContextMenu />}
          </div>

          <div className="flex-1 flex justify-between pr-2">
            <div className="relative">
              <FaSearch className="absolute left-3 top-3 h-4 w-4 text-gray-500 " />
              <CommandShortcut className="absolute right-3 top-3 h-4 w-4 text-gray-500 ">
                ⌘K
              </CommandShortcut>
              <Input
                className="w-full bg-white shadow-none appearance-none pl-8"
                onClick={(e) => {
                  e.stopPropagation();
                  e.preventDefault();
                  setDocsSearchShowDialog(true);
                }}
                placeholder="Search all docs..."
                type="search"
                value={docsSearchText}
              />
            </div>

            <div>
              <ServerField />
            </div>
          </div>
        </div>

        <main
          className="flex w-full flex-1 overflow-hidden"
          ref={mainRef}
          style={{ height: `${mainHeightToBottomOfScreen}px` }}
        >
          <RPCNavMenu />

          <div
            className="flex-1 overflow-y-scroll p-4"
            style={{ height: `${mainHeightToBottomOfScreen}px` }}
          >
            {isLoadingDocs ? (
              <div className="flex flex-col items-center justify-center space-y-4 h-full">
                <ImSpinner10 className="animate-spin" size={36} />
                <div className="text-gray-500 text-lg">Loading Docs...</div>
              </div>
            ) : (
              children
            )}
          </div>
        </main>
      </div>
    </>
  );
}

DashboardLayout.defaultProps = {};

export { DashboardLayout };
