import { useEffect, useState } from 'react';
import { MonacoEditor } from './MonacoEditor';
import { quicktypeJSONSchema } from "@/util/quicktype.ts";

interface Props {
  jsonSchema: any;
  jsonSchemaTypeName?: string;
  schemaLanguage: string;
}

export function JSONSchemaDisplayer({
  jsonSchema,
  jsonSchemaTypeName: typeName,
  schemaLanguage,
}: Props) {
  // const [data, setData] = useState<any[]>([]);
  const [text, setText] = useState(JSON.stringify(jsonSchema, null, 2));

  useEffect(() => {
    if (schemaLanguage !== 'json') {
      quicktypeJSONSchema({
        targetLanguage: schemaLanguage,
        typeName: typeName || 'JsonSchemaType',
        jsonSchemaString: JSON.stringify(jsonSchema),
      }).then((result) => {
        // setData(result.lines);
        setText(result.lines.join('\n'));
      });
    } else {
      // setData([]);
      setText(JSON.stringify(jsonSchema, null, 2));
    }
  }, [schemaLanguage, jsonSchema]);

  return (
    <div>
      {/*<CopyJSONSchema*/}
      {/*  schemaLanguage={schemaLanguage}*/}
      {/*  textToCopy={text}*/}
      {/*/>*/}
      <div className="rounded overflow-hidden">
        {text?.length > 0 && (
          <MonacoEditor language={schemaLanguage} text={text} />
        )}
      </div>
    </div>
  );
}
