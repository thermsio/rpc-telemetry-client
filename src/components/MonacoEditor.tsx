import { useEffect, useState } from 'react';
import Editor from '@monaco-editor/react';
import theme from './MonacoEditorTheme.json'

interface Props {
  language: string;
  text: string;
}

export const MonacoEditor = ({ language, text }: Props) => {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    // we want the editor to refresh when the `text` prop changes, so we use
    // this work-around to show loading which unmounts the editor for a few
    // miliseconds
    setLoading(true);
  }, [text]);

  useEffect(() => {

    if (loading) {
      const t = setTimeout(() => {
        setLoading(false);
      }, 500);

      return () => clearTimeout(t);
    }
  }, [loading]);

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <Editor
        onMount={(_editor, monaco) => {
          monaco.editor.defineTheme("dark", theme as any)
          monaco.editor.setTheme("dark")
        }}
        options={{
          automaticLayout: true,
          lineNumbers: 'on',
          readOnly: true,
        }}
        height="500px"
        language={language}
        theme={'light'}
        value={text}
      />
    </div>
  );
};
