import { useDocsStore } from '@/state/docs.ts';
import { Badge } from '@/components/ui/badge.tsx';
import { useNavigate } from 'react-router-dom';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { toast } from 'sonner';
import { FaCopy } from 'react-icons/fa';
import { JSONSchemaDisplayer } from '@/components/JSONSchemaDisplayer.tsx';
import { Tabs, TabsContent, TabsList, TabsTrigger } from './ui/tabs';
import { Button } from './ui/button';
import { copyRpcDocsToClipboard } from '@/util/copy-rpc-docs-to-clipboard.ts';

interface RPCProcedureDetailsProps {
  procedure: string;
  scope: string;
  version?: string;
}

function RPCProcedureDetails({
  procedure,
  scope,
  version,
}: RPCProcedureDetailsProps) {
  const navigate = useNavigate();
  const { docsViewState, rpcsByScope, setDocsViewState } = useDocsStore();

  const rpc = rpcsByScope[scope]?.find((rpc) => rpc.procedure === procedure);

  const versions = rpc?.versions;
  const currentVersion = versions?.find((v) => v.version === version);

  if (!rpc || !versions || !currentVersion) {
    return (
      <div className="flex space-x-2 bg-red-200 text-red-800 p-4 rounded-md text-sm ">
        <span className="font-bold">RPC not found in docs</span>
        <pre className="font-light">
          {scope}::{procedure}::{version}
        </pre>
      </div>
    );
  }

  return (
    <div className="flex flex-col space-y-4">
      <div className="flex items-center group hover:cursor-pointer w-fit">
        <CopyToClipboard
          text={`${scope}::${procedure}::${version}`}
          onCopy={() => {
            toast('RPC copied to clipboard');
          }}
        >
          <h1 className="font-semibold text-xl">
            {scope}::{procedure}::{version}
            <div className="hidden group-hover:inline ml-1">
              <FaCopy className="text-gray-400 inline-block" />
            </div>
          </h1>
        </CopyToClipboard>
      </div>

      {versions.length > 1 && (
        <div className="flex space-x-2">
          <span className="text-gray-500 font-light">Versions</span>
          {versions.map((v) => (
            <Badge
              className="cursor-pointer"
              key={v.version}
              onClick={() => {
                navigate(`/rpc/${scope}/${procedure}/${v.version}`);
              }}
              variant={v.version === version ? 'default' : 'outline'}
            >
              {v.version}
            </Badge>
          ))}
        </div>
      )}

      {currentVersion.description && (
        <div className="bg-gray-200 rounded p-2 text-gray-600">
          {currentVersion.description}
        </div>
      )}

      {(currentVersion.args || currentVersion.data) && (
        <div>
          <Tabs value={docsViewState.rpcDetailsTab} className="">
            <TabsList className="grid w-full grid-cols-2">
              <TabsTrigger
                value="args"
                onClick={() => setDocsViewState({ rpcDetailsTab: 'args' })}
              >
                Args
              </TabsTrigger>
              <TabsTrigger
                value="data"
                onClick={() => setDocsViewState({ rpcDetailsTab: 'data' })}
              >
                Data
              </TabsTrigger>
            </TabsList>

            <Tabs value={docsViewState.lang}>
              <TabsList className="grid w-full grid-cols-5">
                <TabsTrigger
                  value="json"
                  onClick={() => setDocsViewState({ lang: 'json' })}
                >
                  JSON-Schema
                </TabsTrigger>
                <TabsTrigger
                  value="typescript"
                  onClick={() => setDocsViewState({ lang: 'typescript' })}
                >
                  TypeScript
                </TabsTrigger>
                <TabsTrigger
                  value="kotlin"
                  onClick={() => setDocsViewState({ lang: 'kotlin' })}
                >
                  Kotlin
                </TabsTrigger>

                <TabsTrigger
                  value="swift"
                  onClick={() => setDocsViewState({ lang: 'swift' })}
                >
                  Swift
                </TabsTrigger>

                <Button
                  onClick={() =>
                    copyRpcDocsToClipboard({
                      rpc: currentVersion,
                      lang: docsViewState.lang,
                      docsToCopy: docsViewState.rpcDetailsTab,
                    })
                      .catch((err) => {
                        toast.error(
                          'Failed to copy to clipboard: ' + err.message,
                        );
                      })
                      .then(() => {
                        toast.success('Copied to clipboard');
                      })
                  }
                  className="mx-2"
                  size="sm"
                  variant="default"
                >
                  <FaCopy className="text-gray-400 inline-block mr-1" />
                  Copy to clipboard
                </Button>
              </TabsList>
            </Tabs>

            <TabsContent value="args">
              <JSONSchemaDisplayer
                jsonSchema={currentVersion.args}
                jsonSchemaTypeName={'args'}
                schemaLanguage={docsViewState.lang}
              />
            </TabsContent>

            <TabsContent value="data">
              <JSONSchemaDisplayer
                jsonSchema={currentVersion.data}
                jsonSchemaTypeName={'data'}
                schemaLanguage={docsViewState.lang}
              />
            </TabsContent>
          </Tabs>
        </div>
      )}
    </div>
  );
}

RPCProcedureDetails.defaultProps = {};

export { RPCProcedureDetails };
