import {
  TableHead,
  TableRow,
  TableHeader,
  TableCell,
  TableBody,
  Table,
} from '@/components/ui/table';
import { useDocsStore } from '@/state/docs.ts';
import { Badge } from '@/components/ui/badge.tsx';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';

interface RPCProcedureTableProps {
  procedure: string;
  scope: string;
}

function RPCProcedureTable({ procedure, scope }: RPCProcedureTableProps) {
  const navigate = useNavigate();
  const { rpcsByScope } = useDocsStore();

  const rpc = rpcsByScope[scope]?.find((rpc) => rpc.procedure === procedure);
  const versions = rpc?.versions;

  useEffect(() => {
    if (!rpc) {
      navigate(`/rpc/${scope}`);
    } else if (versions?.length === 1) {
      navigate(`/rpc/${scope}/${procedure}/${rpc.latestVersion.version}`);
    }
  }, [rpc]);

  if (!rpc || !versions || versions?.length === 1) {
    return null;
  }

  return (
    <div className="flex flex-col space-y-4">
      <div className="flex items-center">
        <h1 className="font-semibold text-xl">
          {scope}::{procedure}
        </h1>
      </div>

      <div className="border shadow-sm rounded-lg">
        <Table>
          <TableHeader>
            <TableRow>
              <TableHead className="w-[50px]">Scope</TableHead>
              <TableHead className="">Procedure</TableHead>
              <TableHead className="hidden md:table-cell">
                Description
              </TableHead>
              <TableHead className="hidden md:table-cell">Version</TableHead>
            </TableRow>
          </TableHeader>
          <TableBody>
            {versions
              .sort((a, b) => (a.version > b.version ? -1 : 1))
              .map((rpcVersion) => (
                <TableRow
                  className="cursor-pointer"
                  key={rpcVersion.version}
                  onClick={() =>
                    navigate(
                      `/rpc/${scope}/${rpcVersion.procedure}/${rpcVersion.version}`,
                    )
                  }
                >
                  <TableCell className="font-semibold">{scope}</TableCell>
                  <TableCell className="font-medium">
                    {rpcVersion.procedure}
                  </TableCell>
                  <TableCell className="hidden md:table-cell">
                    {rpcVersion.description}
                  </TableCell>
                  <TableCell className="hidden md:table-cell">
                    <Badge
                      variant={
                        rpcVersion.version === rpc.latestVersion.version
                          ? 'default'
                          : 'outline'
                      }
                    >
                      {rpcVersion.version}
                    </Badge>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </div>
    </div>
  );
}

RPCProcedureTable.defaultProps = {};

export { RPCProcedureTable };
