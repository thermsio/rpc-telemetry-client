import {
  TableHead,
  TableRow,
  TableHeader,
  TableCell,
  TableBody,
  Table,
} from '@/components/ui/table';
import { useDocsStore } from '@/state/docs.ts';
import { Badge } from '@/components/ui/badge.tsx';
import { useNavigate } from 'react-router-dom';

interface RPCScopeTableProps {
  scope: string;
}

function RPCScopeTable({ scope }: RPCScopeTableProps) {
  const navigate = useNavigate();
  const { rpcsByScope } = useDocsStore();

  if (!rpcsByScope[scope]) {
    return null;
  }

  return (
    <div className="flex flex-col space-y-4">
      <div className="flex items-center">
        <h1 className="font-semibold text-xl">{scope}</h1>
      </div>

      <div className="border shadow-sm rounded-lg">
        <Table>
          <TableHeader>
            <TableRow>
              <TableHead className="w-[50px]">Scope</TableHead>
              <TableHead className="">Procedure</TableHead>
              <TableHead className="hidden md:table-cell">
                Description
              </TableHead>
              <TableHead className="hidden md:table-cell">Version</TableHead>
            </TableRow>
          </TableHeader>
          <TableBody>
            {rpcsByScope[scope].map((rpc) => (
              <TableRow
                className="cursor-pointer"
                key={rpc.procedure}
                onClick={() =>
                  navigate(
                    `/rpc/${scope}/${rpc.procedure}/${rpc.latestVersion.version}`,
                  )
                }
              >
                <TableCell className="font-semibold">{scope}</TableCell>
                <TableCell className="font-medium">{rpc.procedure}</TableCell>
                <TableCell className="hidden md:table-cell">
                  {rpc.latestVersion.description}
                </TableCell>
                <TableCell className="hidden md:table-cell">
                  {rpc.versions
                    .sort((a, b) => (a.version > b.version ? -1 : 1))
                    .map((rpcVersion) => (
                      <Badge
                        variant={
                          rpc.latestVersion.version === rpcVersion.version
                            ? 'default'
                            : 'outline'
                        }
                      >
                        {rpcVersion.version}
                      </Badge>
                    ))}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    </div>
  );
}

RPCScopeTable.defaultProps = {};

export { RPCScopeTable };
