import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
  DropdownMenuItem,
} from '@/components/ui/dropdown-menu';
import { FaHistory, FaTimes } from 'react-icons/fa';
import { Button } from '@/components/ui/button.tsx';
export function RPCServerHistoryDropdown({
  options,
  onRemove,
  onSelect,
}: {
  options: string[];
  onRemove: (value: string) => void;
  onSelect: (value: string) => void;
}) {
  return (
    <DropdownMenu>
      <DropdownMenuTrigger className="bg-white p-1.5">
        <FaHistory />
      </DropdownMenuTrigger>
      <DropdownMenuContent onSelect={console.log}>
        {options.map((value) => (
          <DropdownMenuItem onSelect={() => onSelect(value)}>
            <div className="flex justify-between items-center w-full">
              <span>{value}</span>

              <div>
                <Button
                  onClick={(e) => {
                    e.stopPropagation();
                    onRemove(value);
                  }}
                  variant="ghost"
                >
                  <FaTimes />
                </Button>
              </div>
            </div>
          </DropdownMenuItem>
        ))}
      </DropdownMenuContent>
    </DropdownMenu>
  );
}
