import { Input } from '@/components/ui/input.tsx';
import { RPCServerHistoryDropdown } from '@/components/RPCServerTextField/components/RPCServerHistoryDropdown.tsx';
import { useDocsStore } from '@/state/docs.ts';
import { FaServer } from "react-icons/fa6";

interface RPCServerTextFieldProps {
  setValue: (value: string) => void;
  value?: string;
}

function RPCServerTextField({ setValue, value }: RPCServerTextFieldProps) {
  const [
    removeRpcServerHistory,
    rpcServerHistory,
  ] = useDocsStore((state) => [
    state.removeRpcServerHistory,
    state.rpcServerHistory,
  ]);

  return (
    <div className="max-w-[300px] w-full relative">
      <FaServer className="absolute left-4 top-3 h-4 w-4 text-gray-500 dark:text-gray-400" />

      <Input
        className="w-full bg-white shadow-none appearance-none pl-10"
        placeholder={'http://localhost:4000'}
        onChange={(e) => {
          setValue(e.target.value);
        }}
        value={value}
      />

      {rpcServerHistory.length > 0 && (
        <div className="absolute right-0 top-0 flex items-center h-full p-2">
          <RPCServerHistoryDropdown
            options={rpcServerHistory}
            onRemove={removeRpcServerHistory}
            onSelect={setValue}
          />
        </div>
      )}
    </div>
  );
}

RPCServerTextField.defaultProps = {};

export { RPCServerTextField };
