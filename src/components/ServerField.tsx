import { FaDownload } from 'react-icons/fa';
import { useDocsStore } from '@/state/docs.ts';
import { useState } from 'react';
import { Button } from '@/components/ui/button.tsx';
import { RPCServerTextField } from '@/components/RPCServerTextField';

function ServerField() {
  const { docsViewState, setDocsViewState } = useDocsStore();
  const [value, setValue] = useState(docsViewState.server);

  return (
    <div className="relative flex space-x-2 min-w-[350px]">
      <RPCServerTextField setValue={setValue} value={value} />
      {value !== docsViewState.server && (
        <Button onClick={() => setDocsViewState({ server: value })}>
          <FaDownload className="mr-1" />
          Load Docs
        </Button>
      )}
    </div>
  );
}

ServerField.defaultProps = {};

export { ServerField };
