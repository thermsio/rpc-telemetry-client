import { DashboardLayout } from '@/components/DashboardLayout';
import { useLocation, useParams } from 'react-router';
import { RPCScopeTable } from '@/components/RPCScopeTable.tsx';
import { RPCProcedureTable } from '@/components/RPCProcedureTable.tsx';
import { RPCProcedureDetails } from '@/components/RPCProcedureDetails.tsx';
import { useEffect } from 'react';
import { useDocsStore } from '@/state/docs.ts';
import { usePrevious } from 'ahooks';

function RPCDocsRoute() {
  const { scope, procedure, version } = useParams<{
    scope?: string;
    procedure?: string;
    version?: string;
  }>();

  const docsStore = useDocsStore();
  const previousDocsStore = usePrevious(docsStore);
  const location = useLocation();
  const previousLocation = usePrevious(location.pathname);

  useEffect(() => {
    docsStore.loadDocs();
  }, []);

  useEffect(() => {
    // we always want a url to be shareable and to be able to be copied and pasted
    // so we sync the state to the URL search params silently
    if (previousLocation !== location.pathname) {
      console.info('syncing docs view state to URL search params');
      window.history.replaceState(
        {},
        '',
        `${location.pathname}?${docsStore.getDocsViewStateUrlSearchParams()}`,
      );
    }
  }, [docsStore.docsViewState, location.pathname]);

  useEffect(() => {
    if (previousDocsStore?.docsViewState !== docsStore.docsViewState) {
      console.info('docs view state changed');
      // we always want a url to be shareable and to be able to be copied and pasted
      window.history.replaceState(
        {},
        '',
        `${location.pathname}?${docsStore.getDocsViewStateUrlSearchParams()}`,
      );

      if (
        previousDocsStore?.docsViewState.server &&
        previousDocsStore?.docsViewState.server !==
          docsStore.docsViewState.server
      ) {
        // trigger reload page when server changes so the user can navigate back in browser history
        return window.location.reload();
      }
    }
  }, [docsStore.docsViewState]);

  return (
    <DashboardLayout>
      {scope && !procedure && <RPCScopeTable scope={scope} />}
      {scope && procedure && !version && (
        <RPCProcedureTable procedure={procedure} scope={scope} />
      )}
      {scope && procedure && version && (
        <RPCProcedureDetails
          procedure={procedure}
          scope={scope}
          version={version}
        />
      )}
    </DashboardLayout>
  );
}

RPCDocsRoute.defaultProps = {};

export { RPCDocsRoute };
