import { useDocsStore } from '@/state/docs.ts';
import { useState } from 'react';
import { Label } from '@/components/ui/label.tsx';
import { Button } from '@/components/ui/button.tsx';
import { RPCServerTextField } from "@/components/RPCServerTextField";

function RootRoute() {
  const [
    docsViewState,
    setDocsViewState,
  ]= useDocsStore(state => [state.docsViewState, state.setDocsViewState]);

  const [value, setValue] = useState(docsViewState.server);

  if (docsViewState.server) {
    window.location.href = '/rpc';
  }

  return (
    <div className="w-screen h-screen bg-gray-200 flex flex-col items-center justify-center space-y-4">
      <div className="text-2xl font-bold">RPC Docs Site</div>
      <Label>Enter an RPC server URL:</Label>
      <RPCServerTextField setValue={setValue} value={value} />

      <div className="flex justify-center">
        <Button
          disabled={!value}
          onClick={() => {
            setDocsViewState({ server: value })
          }}
        >
          Load RPC Docs
        </Button>
      </div>
    </div>
  );
}

RootRoute.defaultProps = {};

export { RootRoute };
