// import docsFixture from './fixtures/rpc-server-docs.json';

type TRPCServerDocsResponse = {
  args?: any;
  data?: any;
  description?: string;
  procedure: string;
  scope: string;
  version: string;
}[];

export const RPCServerService = {
  getDocs: async (rpcServerUrl: string): Promise<TRPCServerDocsResponse> => {
    // return docsFixture
    const response = await fetch(`${rpcServerUrl}/docs`, {
      headers: {
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      method: 'GET',
    });
    const json = await response.json().catch((e) => {
      throw new Error(
        'RPCServerService.getDocs() response JSON error:' + e.message,
      );
    });

    return json;
  },
};
