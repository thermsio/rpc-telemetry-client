import { create } from 'zustand';
import { RPCServerService } from '@/services/rpc-server.ts';
import MiniSearch, { SearchResult } from 'minisearch';
import { makeRpcsByScope } from '@/state/utils/make-rpcs-by-scope.ts';
import { addAdditionalPropertiesFalse } from '@/util/json-docs-add-additional-properties-false.ts';

export type TDocsViewState = {
  docsSearch?: string;
  server?: string;
  rpcDetailsTab: 'args' | 'data';
  rpcFilter?: string;
  lang: 'typescript' | 'json' | 'kotlin' | 'swift';
};

export const DOCS_VIEW_STATE_STORAGE_KEY = 'docs-view-state';
export const RPC_SERVER_HISTORY_STATE_STORAGE_KEY = 'rpc-server-history';

export type TRPCDoc = {
  args?: Record<any, unknown>;
  data?: Record<any, unknown>;
  description?: string;
  id?: string;
  procedure: string;
  scope: string;
  version: string;
};

export type TRPCSearchDoc = {
  args?: string;
  data?: string;
  description?: string;
  id?: string;
  procedure: string;
  scope: string;
  version: string;
};

export type TRPCVersions = {
  latestVersion: TRPCDoc;
  procedure: string;
  scope: string;
  versions: TRPCDoc[];
};

// type RPCVersionsGroup = Record<string, TRPCVersions>;

export type TRPCsByScope = Record<TRPCDoc['scope'], TRPCVersions[]>;

export type TDocsStore = {
  addRpcServerHistory: (value: string) => void;
  removeRpcServerHistory: (value: string) => void;
  docsSearchLoading: boolean;
  docsSearchResults: (SearchResult & TRPCDoc)[];
  docsSearchShowDialog: boolean;
  docsSearchText?: string;
  docsViewState: TDocsViewState;
  getDocsViewStateUrlSearchParams: () => URLSearchParams;
  isLoadingDocs: boolean;
  loadDocs: () => Promise<void>;
  loadDocsError?: { message: string };
  rpcsById: Record<string, TRPCDoc>;
  rpcsByScope: TRPCsByScope;
  rpcDocs: TRPCDoc[];
  rpcServerHistory: string[];
  setDocsSearchShowDialog: (show: boolean) => void;
  setDocsSearchText: (text: string) => void;
  setDocsViewState: (state: Partial<TDocsViewState>) => void;
};

const docsViewStateFromSearch = new URLSearchParams(window.location.search);

if (docsViewStateFromSearch) {
  const searchParamsForDocsViewState: Partial<TDocsViewState> = {};

  if (docsViewStateFromSearch.has('server')) {
    searchParamsForDocsViewState.server = docsViewStateFromSearch.get(
      'server',
    ) as TDocsViewState['server'];
  }

  if (docsViewStateFromSearch.has('rpcDetailsTab')) {
    searchParamsForDocsViewState.rpcDetailsTab = docsViewStateFromSearch.get(
      'rpcDetailsTab',
    ) as TDocsViewState['rpcDetailsTab'];
  }

  if (docsViewStateFromSearch.has('lang')) {
    searchParamsForDocsViewState.lang = docsViewStateFromSearch.get(
      'lang',
    ) as TDocsViewState['lang'];
  }

  if (Object.keys(searchParamsForDocsViewState).length) {
    try {
      localStorage.setItem(
        DOCS_VIEW_STATE_STORAGE_KEY,
        JSON.stringify(searchParamsForDocsViewState),
      );
    } catch (e: any) {
      console.warn(
        'Failed to set docs view state in session storage from search params: ' +
          window.location.search,
        e.message,
      );
    }
  }
} else {
  // load from session storage
  const docsViewStateSessionStorage = localStorage.getItem(
    DOCS_VIEW_STATE_STORAGE_KEY,
  );

  if (docsViewStateSessionStorage) {
    try {
      const docsViewState = JSON.parse(
        docsViewStateSessionStorage,
      ) as TDocsViewState;
      if (!docsViewState.docsSearch) {
        docsViewState.docsSearch = '';
      }
      if (!docsViewState.server) {
        docsViewState.server = '';
      }
      if (!docsViewState.rpcDetailsTab) {
        docsViewState.rpcDetailsTab = 'args';
      }
      if (!docsViewState.lang) {
        docsViewState.lang = 'typescript';
      }
      localStorage.setItem(
        DOCS_VIEW_STATE_STORAGE_KEY,
        JSON.stringify(docsViewState),
      );
    } catch (e: any) {
      console.warn(
        'Failed to parse docs view state from session storage',
        e.message,
      );
      localStorage.removeItem(DOCS_VIEW_STATE_STORAGE_KEY);
    }
  }
}

function getInitialDocsViewState() {
  let initialDocsViewState: TDocsViewState = {
    lang: 'typescript',
    rpcDetailsTab: 'args',
    server: '',
  };
  try {
    return {
      ...initialDocsViewState,
      ...(JSON.parse(
        localStorage.getItem(DOCS_VIEW_STATE_STORAGE_KEY) || '{}',
      ) as TDocsViewState),
    };
  } catch (e: any) {
    console.warn(
      'Failed to parse docs view state from session storage',
      e.message,
    );
    localStorage.removeItem(DOCS_VIEW_STATE_STORAGE_KEY);

    return initialDocsViewState;
  }
}

function getInitialRpcServerHistory() {
  let initialRpcServerHistory: string[] = [];
  try {
    return JSON.parse(
      localStorage.getItem(RPC_SERVER_HISTORY_STATE_STORAGE_KEY) || '[]',
    );
  } catch (e: any) {
    console.warn(
      'Failed to parse rpc server history from session storage',
      e.message,
    );
    localStorage.removeItem(RPC_SERVER_HISTORY_STATE_STORAGE_KEY);

    return initialRpcServerHistory;
  }
}

const miniSearch = new MiniSearch<TRPCSearchDoc>({
  fields: ['scope', 'procedure', 'description', 'args', 'data', 'version'],
  storeFields: ['procedure', 'scope', 'description', 'args', 'data', 'version'],
  searchOptions: {
    prefix: true,
    fuzzy: 0.2,
  },
});

export const useDocsStore = create<TDocsStore>((set, get) => ({
  addRpcServerHistory: (value) => {
    set((prev) => {
      const rpcServerHistory = [...prev.rpcServerHistory].filter(
        (v) => v !== value,
      );

      rpcServerHistory.unshift(value);

      localStorage.setItem(
        RPC_SERVER_HISTORY_STATE_STORAGE_KEY,
        JSON.stringify(rpcServerHistory),
      );

      return { rpcServerHistory };
    });
  },
  removeRpcServerHistory: (value) => {
    set((prev) => {
      const rpcServerHistory = prev.rpcServerHistory.filter((v) => v !== value);
      localStorage.setItem(
        RPC_SERVER_HISTORY_STATE_STORAGE_KEY,
        JSON.stringify(rpcServerHistory),
      );
      return { rpcServerHistory };
    });
  },
  docsSearchLoading: false,
  docsSearchShowDialog: false,
  docsSearchText: '',
  docsSearchResults: [],
  docsViewState: getInitialDocsViewState(),
  getDocsViewStateUrlSearchParams: () => {
    const searchParams = new URLSearchParams();

    const docsViewStateSessionStorage = localStorage.getItem(
      DOCS_VIEW_STATE_STORAGE_KEY,
    );

    if (docsViewStateSessionStorage) {
      const docsViewState = JSON.parse(
        docsViewStateSessionStorage,
      ) as TDocsViewState;
      searchParams.set('docsSearch', docsViewState.docsSearch || '');
      searchParams.set('server', docsViewState.server || '');
      searchParams.set('rpcDetailsTab', docsViewState.rpcDetailsTab || '');
      searchParams.set('rpcFilter', docsViewState.rpcFilter || '');
      searchParams.set('lang', docsViewState.lang || '');
    }

    return searchParams;
  },
  isLoadingDocs: false,
  loadDocs: async () => {
    const server = get().docsViewState.server;

    if (!server) {
      return;
    }

    set({ isLoadingDocs: true, loadDocsError: undefined });

    return RPCServerService.getDocs(server)
      .then((docs: TRPCDoc[]) => {
        // make all objects have additionalProperties: false
        addAdditionalPropertiesFalse(docs);

        const rpcsByScope = makeRpcsByScope(docs);

        const rpcsById = docs.reduce(
          (acc, rpc) => {
            const id = `${rpc.scope}::${rpc.procedure}::${rpc.version}`;
            acc[id] = {
              ...rpc,
              id,
            };

            return acc;
          },
          {} as TDocsStore['rpcsById'],
        );

        set({ rpcsById, rpcsByScope, rpcDocs: docs });

        const searchDocs: TRPCSearchDoc[] = Object.values(rpcsById).map(
          (rpc) => ({
            ...rpc,
            args: JSON.stringify(rpc.args),
            data: JSON.stringify(rpc.data),
          }),
        );
        miniSearch.removeAll();
        miniSearch.addAll(searchDocs);
      })
      .catch((err) => {
        console.error('RPCServerService.getDocs error', err.message);
        set({ loadDocsError: err.message });
      })
      .finally(() => {
        set({ isLoadingDocs: false });
      });
  },
  loadDocsError: undefined,
  rpcsById: {},
  rpcsByScope: {},
  rpcDocs: [],
  rpcServerHistory: getInitialRpcServerHistory(),
  setDocsSearchShowDialog: (show) => {
    set({ docsSearchShowDialog: show });
  },
  setDocsSearchText: async (text) => {
    set({ docsSearchText: text });

    const searchResults = await new Promise<TDocsStore['docsSearchResults']>(
      (r) => {
        const results = miniSearch.search(text || '');
        r(results as TDocsStore['docsSearchResults']);
      },
    );

    set({
      docsSearchResults: searchResults,
    });
  },
  setDocsViewState: (state) => {
    set((prev) => {
      const newState = { ...prev.docsViewState, ...state };

      localStorage.setItem(
        DOCS_VIEW_STATE_STORAGE_KEY,
        JSON.stringify(newState),
      );

      if (newState.server) {
        get().addRpcServerHistory(newState.server);
      }

      return { docsViewState: newState };
    });
  },
}));

// useDocsStore.subscribe((state, previous) => {
//
// });
