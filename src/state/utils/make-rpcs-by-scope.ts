import { groupBy } from 'lodash';
import { TRPCDoc, TRPCsByScope } from '@/state/docs.ts';

export function makeRpcsByScope(docs: TRPCDoc[]): TRPCsByScope {
  const scopeGrouped = groupBy(docs, 'scope');

  const rpcsByScope = Object.entries(scopeGrouped).reduce(
    (acc, [scope, rpcs]) => {
      const procedureGrouped = groupBy(rpcs, 'procedure');

      acc[scope] = Object.entries(procedureGrouped).map(
        ([procedure, versions]) => {
          const sortedVersions = versions.sort((a, b) =>
            a.version.localeCompare(b.version),
          );

          const latestVersion = sortedVersions[sortedVersions.length - 1];

          return {
            latestVersion,
            procedure,
            scope,
            versions: sortedVersions,
          };
        },
      );

      return acc;
    },
    {} as TRPCsByScope,
  );

  return rpcsByScope;
}
