import { TRPCDoc } from '../state/docs.ts';
import { quicktypeJSONSchema } from '@/util/quicktype.ts';

export async function copyRpcDocsToClipboard({
  rpc,
  lang,
  docsToCopy,
}: {
  rpc: TRPCDoc;
  lang: string;
  docsToCopy: 'args' | 'data';
}) {
  const convertedJsonToLang = await quicktypeJSONSchema({
    targetLanguage: lang,
    typeName: `${rpc.scope}-${rpc.procedure}-v${rpc.version}-args`,
    jsonSchemaString: JSON.stringify(rpc[docsToCopy]),
  });

  return navigator.clipboard.writeText(convertedJsonToLang.lines.join('\n'));
}
