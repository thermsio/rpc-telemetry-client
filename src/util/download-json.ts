import { TRPCDoc } from '@/state/docs.ts';

export async function downloadJson(rpcDocs: TRPCDoc[]) {
  if (!Array.isArray(rpcDocs)) {
    throw new Error(
      'downloadJSONSchema(rpcDocs) did not receive "rpcDocs" array',
    );
  } else if (rpcDocs.length === 0) {
    throw new Error('downloadJSONSchema(rpcDocs) received an empty array');
  }

  try {
    let file = JSON.stringify(
      rpcDocs.map((rpc) => ({
        args: rpc.args,
        data: rpc.data,
        description: rpc.description,
        version: rpc.version,
        scope: rpc.scope,
        procedure: rpc.procedure,
      })),
      null,
      2,
    );

    const element = document.createElement('a');

    element.setAttribute(
      'href',
      'data:text/plain;charset=utf-8,' + encodeURIComponent(file),
    );
    element.setAttribute('download', 'rpc-docs-schema.json');

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  } catch (err) {
    console.warn(
      'downloadJSONSchema(rpcDocs) errored trying to generate and download.',
      rpcDocs,
    );
    console.error(err);
    alert(
      'Unable to convert and download schema, check console for more details.',
    );
  }
}
