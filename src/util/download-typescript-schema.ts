import { startCase } from 'lodash';
import { TRPCsByScope } from '@/state/docs.ts';
import { quicktypeJSONSchema } from '@/util/quicktype.ts';

export async function downloadTypescriptSchema(rpcsByScope: TRPCsByScope) {
  if (typeof rpcsByScope !== 'object') {
    throw new Error(
      'downloadTypescriptSchema(rpcsByScope) did not receive "rpcsByScope" object',
    );
  }

  let file = `
/** 
    This file was automatically generated from RPC-telemetry-client.
    ${window.location.href} 
    Generated at ${new Date().toString()}
**/

import { RPCClient, CallOptions } from '@therms/rpc-client'

export namespace RPCDocs {
`;

  // { [scope: string]: { [procedure: string]: { [version: string]: string } } }
  // at the end of this file we append the constants with the RPC shorthand reference, ie: `${scope}::${procedure}::${version.version}`
  const rpcMap: Record<string, Record<string, Record<string, string>>> = {};

  const rpcsScopeAndProcedureVersions = Array.from(Object.entries(rpcsByScope));

  try {
    for await (const [scope, rpcVersions] of rpcsScopeAndProcedureVersions) {
      const scopeFormatted = startCase(scope).split(' ').join('');

      if (!rpcMap[scopeFormatted]) {
        rpcMap[scopeFormatted] = {};
      }

      file += `export namespace ${scopeFormatted} {\n`;

      for await (const rpc of rpcVersions) {
        const { procedure, versions } = rpc;
        const procedureFormatted = startCase(procedure).split(' ').join('');

        if (!rpcMap[scopeFormatted][procedureFormatted]) {
          rpcMap[scopeFormatted][procedureFormatted] = {};
        }

        file += `export namespace ${procedureFormatted} {\n`;

        for await (const version of versions) {
          if (
            !rpcMap[scopeFormatted][procedureFormatted][`v${version.version}`]
          ) {
            rpcMap[scopeFormatted][procedureFormatted][`v${version.version}`] =
              `${scope}::${procedure}::${version.version}`;
          }

          file += `export namespace v${version.version} {\n`;

          if (version.args?.type === 'object' && !version.args?.properties) {
            file += `export type Args = object;\n`;
          } else if (version.args) {
            const { lines } = await quicktypeJSONSchema({
              targetLanguage: 'typescript',
              typeName: `Args`,
              jsonSchemaString: JSON.stringify(version.args),
            }).catch((err) => {
              console.warn(
                `quicktypeJSONSchema() unable to convert: ${scope}::${procedure}::v${version.version}::Args`,
                {
                  scope,
                  procedure,
                  version,
                },
              );

              throw err;
            });

            file += lines.join('\n') + '\n';
          } else {
            file += `export type Args = unknown;\n`;
          }

          if (version.data?.type === 'object' && !version.data?.properties) {
            file += `export type Data = object;\n`;
          } else if (version.data) {
            const { lines } = await quicktypeJSONSchema({
              targetLanguage: 'typescript',
              typeName: `Data`,
              jsonSchemaString: JSON.stringify(version.data),
            }).catch((err) => {
              console.warn(
                `quicktypeJSONSchema() unable to convert: ${scope}::${procedure}::v${version.version}::Data`,
                {
                  scope,
                  procedure,
                  version,
                },
              );

              throw err;
            });

            file += lines.join('\n') + '\n';
          } else {
            file += `export type Data = void;\n`;
          }

          file += `} // end of v${version.version} namespace\n\n`;
        }

        file += `} // end of ${procedureFormatted} namespace\n\n`;
      }

      file += `} // end of ${scopeFormatted} namespace\n\n`;
    }

    file += '}\n';

    file += `\n\n`;

    file += `

// RPC call shorthand constants

export const RPCs = ${JSON.stringify(rpcMap, null, 2)}       
`;

    let rpcPrebuiltCaller = `
/**
  The "rpc" export is code-generated with all args/data typings constructed for a great autocomplete DX in any TS project. 
  
  The "rpc" calls rely on an instance of the RPCClient constructor that is exported from npm pkg "@therms/rpc-client".
**/
 
export type CallReturnType<Data = unknown> = Promise<{
  code: number
  data: Data
  message?: string
  success: boolean
}>

export const RPCService = (rpcClient: RPCClient) => ({
`;

    Object.keys(rpcMap).forEach((scope) => {
      rpcPrebuiltCaller += `${scope}: {`;

      Object.keys(rpcMap[scope]).forEach((procedure) => {
        rpcPrebuiltCaller += `${procedure}: {`;

        Object.keys(rpcMap[scope][procedure]).forEach((version) => {
          rpcPrebuiltCaller += `${version}: {
    call: (args: RPCDocs.${scope}.${procedure}.${version}.Args, opts?: CallOptions): CallReturnType<RPCDocs.${scope}.${procedure}.${version}.Data> => rpcClient.call(RPCs.${scope}.${procedure}.${version}, args, opts),
},\n
`;
        });

        rpcPrebuiltCaller += `},\n`;
      });

      rpcPrebuiltCaller += `},\n`;
    });

    rpcPrebuiltCaller += `}) // RPCService`;

    file += `\n\n`;
    file += rpcPrebuiltCaller;

    const element = document.createElement('a');

    element.setAttribute(
      'href',
      'data:text/plain;charset=utf-8,' + encodeURIComponent(file),
    );
    element.setAttribute('download', 'rpc-docs-schema.ts');

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  } catch (err) {
    console.warn(
      'downloadTypescriptSchema(rpcsByScope) unable to convert using quicktype. Be sure that the passed in data "rpcsByScope" didnt change unexpectedly. ',
      rpcsByScope,
    );
    console.error(err);
    alert(
      'Unable to convert and download schema, check console for more details.',
    );
  }
}
