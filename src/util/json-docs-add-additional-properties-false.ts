/**
 * This function will recursively search for JSON-schema { type: 'object' } and add { additionalProperties: false } to them.
 * @param data
 */
export function addAdditionalPropertiesFalse(data: any) {
  // If the current data is an object and not an array, proceed
  if (typeof data === 'object' && !Array.isArray(data)) {
    // Check if the object has the type property 'object'
    if (data.type === 'object') {
      // Add the additionalProperties property set to false
      data.additionalProperties = false;
    }

    // Iterate over each property of the object
    for (let key in data) {
      // If the property is an object or an array, recurse into it
      if (typeof data[key] === 'object') {
        addAdditionalPropertiesFalse(data[key]);
      }
    }
  } else if (Array.isArray(data)) {
    // Iterate over each item of the array
    for (let i = 0; i < data.length; i++) {
      // If the item is an object or an array, recurse into it
      if (typeof data[i] === 'object') {
        addAdditionalPropertiesFalse(data[i]);
      }
    }
  }
}
