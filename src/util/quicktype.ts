import {
  quicktype,
  InputData,
  JSONSchemaInput,
  FetchingJSONSchemaStore,
} from 'quicktype-core';
// import { acronymStyle } from 'quicktype-core/dist/support/Acronyms';

interface Params {
  targetLanguage: string;
  typeName: string;
  jsonSchemaString: string;
}

export async function quicktypeJSONSchema({
  targetLanguage,
  typeName,
  jsonSchemaString,
}: Params) {
  if (!targetLanguage) {
    throw new Error('arg "targetLanguage" is required');
  }

  if (!typeName) {
    throw new Error('arg "typeName" is required');
  }

  if (typeof jsonSchemaString !== 'string') {
    throw new Error(
      'arg "jsonSchemaString" must be a string, did you mean to pass a JSON.stringified() version of an object?',
    );
  }

  const schemaInput = new JSONSchemaInput(new FetchingJSONSchemaStore());

  // We could add multiple schemas for multiple types,
  // but here we're just making one type from JSON schema.
  await schemaInput.addSource({ name: typeName, schema: jsonSchemaString });

  const inputData = new InputData();
  inputData.addInput(schemaInput);

  return await quicktype({
    inputData,
    lang: targetLanguage,
    alphabetizeProperties: true,
    rendererOptions: {
      framework: 'just-types', // kotlin
      package: ' ', // kotlin
      'just-types': true, // swift & typescript
      'acronym-style': 'original', // swift
    },
  });
}
